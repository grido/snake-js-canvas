(function () {
    var Food = (function () {
        var Food = function (collisionsObject, board, size) {
            this.boardWidth = board.width;
            this.boardHeight = board.height;
            this.size = size;

            this.position = {};

            this.make(collisionsObject);
        };

        Food.prototype.make = function (collisionsObject) {
            this.position = null;

            while (this.position === null) {
                this.position = {
                    x: randPosition.call(this, this.boardWidth),
                    y: randPosition.call(this, this.boardHeight)
                };

                collisionsObject.forEach((function (part) {
                    if (this.position && this.position.x === part.position.x && this.position.y === part.position.y) {
                        this.position = null;
                    }
                }).bind(this));
            }
        };

        var randPosition = function (max) {
            var position = Math.floor(Math.random() * (max - this.size));

            return position - (position % this.size);
        };

        return Food;
    })();

    var Body = (function () {
        var Body = function (position, direction, size) {
            this.position = {x: position.x, y: position.y};
            this.direction = {axis: direction.axis, value: direction.value};
            this.currentAxis = direction.axis;
            this.size = size;
        };

        Body.prototype.setDirection = function (directionAxis, directionValue) {
            if (directionAxis !== this.currentAxis) {
                this.direction = {axis: directionAxis, value: directionValue};
            }
        };

        return Body;
    })();

    var Snake = (function () {
        var Snake = function (board, options) {
            this.health = true;
            this.score = 0;
            this.best = window.localStorage.getItem('best');
            this.boardWidth = board.width;
            this.boardHeight = board.height;

            var position = {x: board.width / 2, y: board.height / 2};
            var direction = {axis: 'x', value: 1};

            this.parts = (new Array(options.length)).fill(undefined).map(function (value, key) {
                return new Body(Object.assign({}, position, {x: position.x - (key * options.size)}), direction, options.size);
            });
        };

        Snake.prototype.move = function () {
            var head = this.parts[0];

            var position = Object.assign({}, head.position);
            head.direction.value ? position[head.direction.axis] += head.size : position[head.direction.axis] -= head.size;

            this.parts.unshift(new Body(position, head.direction, head.size));

            checkOrUpdateHealth.call(this);
        };

        Snake.prototype.eat = function (food) {
            var head = this.parts[0];

            if (food.position.x === head.position.x && food.position.y === head.position.y) {
                updateResult.call(this);

                food.make(this.parts);
            } else {
                this.parts.pop();
            }
        };

        Snake.prototype.turn = function (keyCode) {
            var head = this.parts[0];

            if (keyCode === 37 || keyCode === 'left') {
                head.setDirection('x', 0);
            } else if (keyCode === 38 || keyCode === 'up') {
                head.setDirection('y', 0);
            } else if (keyCode === 39 || keyCode === 'right') {
                head.setDirection('x', 1);
            } else if (keyCode === 40 || keyCode === 'down') {
                head.setDirection('y', 1);
            }
        };

        var checkOrUpdateHealth = function () {
            var head = this.parts[0];

            var collisionWithSelf = this.parts.find(function (part, key) {
                    return key > 0 && part.position.x === head.position.x && part.position.y === head.position.y;
                }) !== undefined;

            var collisionsWithWall = {
                left: head.position.x < 0,
                top: head.position.y < 0,
                right: head.position.x >= this.boardWidth,
                bottom: head.position.y >= this.boardHeight
            };

            this.health = !collisionWithSelf && !collisionsWithWall.left && !collisionsWithWall.top && !collisionsWithWall.right && !collisionsWithWall.bottom;
        };

        var updateResult = function () {
            this.score++;

            if (!this.best || this.score > this.best) {
                this.best = this.score;
                window.localStorage.setItem('best', this.score);
            }
        };

        return Snake;
    })();

    var Board = (function () {
        var Board = function () {
            this.canvas = document.querySelector('canvas');
            this.screens = document.querySelectorAll('.screen');
            this.scores = document.querySelectorAll('.score');
            this.bests = document.querySelectorAll('.best');
            this.context = this.canvas.getContext('2d');
            this.width = null;
            this.height = null;

            this.resize();
        };

        Board.prototype.draw = function (snake, food) {
            this.context.clearRect(0, 0, this.width, this.height);
            this.context.fillStyle = '#fff';
            this.context.strokeStyle = '#403e39';
            snake.parts.forEach((function (part) {
                this.context.fillRect(part.position.x, part.position.y, part.size, part.size);
                this.context.strokeRect(part.position.x, part.position.y, part.size, part.size);
            }).bind(this));

            this.context.fillRect(food.position.x, food.position.y, food.size, food.size);
            this.context.strokeRect(food.position.x, food.position.y, food.size, food.size);
        };

        Board.prototype.toggleScreen = function (screenName) {
            this.screens.forEach(function (screen) {
                screenName && screen.classList.contains(screenName) ? screen.classList.add('active') : screen.classList.remove('active');
            });
        };

        Board.prototype.showResult = function (score, best) {
            (new Array()).forEach.call(arguments, (function (result, key) {
                var elements = null;

                switch (key) {
                    case 0:
                        elements = this.scores;
                        break;
                    case 1:
                        elements = this.bests;
                        break;
                }

                elements.forEach(function (element) {
                    element.innerHTML = result ? result : 0;
                })
            }).bind(this));
        };

        Board.prototype.resize = function () {
            if(window.innerWidth > 804) {
                this.width = this.canvas.width = 800;
                this.height = this.canvas.height = 600;
            } else if(window.innerWidth > 604) {
                this.width = this.canvas.width = 600;
                this.height = this.canvas.height = 400;
            } else {
                this.width = this.canvas.width = 320;
                this.height = this.canvas.height = 360;
            }
        };

        return Board;
    })();

    var Game = (function () {
        var Game = function (options) {
            this.options = options;
            this.board = new Board();
            this.snake = null;
            this.food = null;
            this.interval = null;
        };

        Game.prototype.init = function () {
            this.board.toggleScreen('start-screen');
            window.addEventListener('keydown', handleKeyboard.bind(this));
            window.addEventListener('click', handleClick.bind(this));
            window.addEventListener('resize', handleResize.bind(this))
        };

        var start = function () {
            this.snake = new Snake(this.board, this.options);
            this.food = new Food(this.snake.parts, this.board, this.options.size);

            this.board.showResult(this.snake.score, this.snake.best);

            this.board.toggleScreen();

            this.interval = setInterval(handleGame.bind(this), this.options.speed);
        };

        var stop = function () {
            this.board.showResult(this.snake.score, this.snake.best);

            this.board.toggleScreen('end-screen');

            clearInterval(this.interval);

            this.interval = null;
        };

        var handleGame = function () {
            this.snake.move();

            this.snake.eat(this.food);

            if (this.snake.health) {
                this.board.showResult(this.snake.score);
                this.board.draw(this.snake, this.food);
            } else {
                stop.call(this);
            }
        };

        var handleKeyboard = function (e) {
            if (this.interval === null) {
                start.call(this);
            }

            if (this.interval) {
                this.snake.turn(e.keyCode);
            }
        };

        var handleClick = function (e) {
            if (this.interval === null && e.target.classList.contains('start-action')) {
                start.call(this);
            }

            if (this.interval && e.target.classList.contains('turn-action')) {
                this.snake.turn(e.target.dataset.direction);
            }
        };

        var handleResize = function () {
            if(this.snake !== null) {
                stop.call(this);
            }

            this.board.resize();
        };

        return Game;
    })();

    var options = {
        size: 20,
        length: 3,
        speed: 120
    };

    var game = new Game(options);

    game.init();
})();